package com.piatrouski.com.ringingnotification;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Base64;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class NotifyWebService extends AsyncTask<String, Void, Void> {
    protected Context _context;

    @Override
    protected Void doInBackground(String... str) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this._context.getApplicationContext());
        String incomingPhoneNumber = str[2];
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        try {
            Phonenumber.PhoneNumber incomingRawPhoneNumber = phoneUtil.parse(incomingPhoneNumber, "ZZ");
            incomingPhoneNumber = phoneUtil.format(incomingRawPhoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
        } catch (NumberParseException e) {
        }

        JSONObject incommingData = new JSONObject();
        try {
            incommingData.put("code", str[0]);
            incommingData.put("type", str[1]);
            incommingData.put("incomming", incomingPhoneNumber);
            incommingData.put("name", this.getContactDisplayNameByNumber(str[2]));

            if (sharedPref.getBoolean("avatar", true)) {
                incommingData.put("avatar", this.getContactPhoto(str[2]));
            } else {
                incommingData.put("avatar", "");
            }

            incommingData.put("message", str[3]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String message = incommingData.toString();

        HttpURLConnection urlConnection = null;
        OutputStream os = null;
        try {
            // @TODO: Add this url to Options page
            URL url = new URL("http://notifica.mobi:8081");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setFixedLengthStreamingMode(message.getBytes().length);
            urlConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            urlConnection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            urlConnection.connect();

            try {
                os = new BufferedOutputStream(urlConnection.getOutputStream());
                os.write(message.getBytes());
                os.flush();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            urlConnection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setContext(Context context) {
        this._context = context;
    }


    protected String getContactDisplayNameByNumber(String number) {
        String name = "Anonymous";
        ContentResolver contentResolver = this._context.getContentResolver();

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Cursor contactLookup = contentResolver.query(uri, new String[]{BaseColumns._ID,
                ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                name = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }
        return name;
    }

    public String getContactPhoto(String number) {
        ContentResolver contentResolver = this._context.getContentResolver();
        Bitmap bitmap = null;

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Cursor contactLookup = contentResolver.query(uri, new String[]{BaseColumns._ID,
                ContactsContract.PhoneLookup.PHOTO_ID, ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI}, null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                if (Build.VERSION.SDK_INT >= 11) {
                    String image_uri = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
                    if (image_uri != null) {
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(contentResolver, Uri.parse(image_uri));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    String id = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Contacts._ID));
                    bitmap = this.loadPhotoBitmap(contentResolver, Long.valueOf(id));
                }
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        if (bitmap != null)
            return "data:image/jpeg;base64," + this.convertBitmapToBase64(bitmap);
        else
            return "";
    }

    public Bitmap loadPhotoBitmap(ContentResolver cr, long id) {
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
        if (input == null) {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }

    public static String convertBitmapToBase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 85, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }
}