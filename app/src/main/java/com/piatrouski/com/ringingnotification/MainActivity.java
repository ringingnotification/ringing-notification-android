package com.piatrouski.com.ringingnotification;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BlurMaskFilter;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextPaint;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import java.math.BigInteger;
import java.security.SecureRandom;

public class MainActivity extends AppCompatActivity {

    protected TextView _codeElement;
    protected Switch _toggleElement;
    protected Switch _toggleAvatarElement;
    protected Button _generateElement;

    protected TextPaint _textPaint;
    protected SharedPreferences _sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        _sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        _generateElement = (Button) findViewById(R.id.button);

        _codeElement = (TextView) findViewById(R.id.code);
        _codeElement.setText(getCode());
        _textPaint = _codeElement.getPaint();
        applyBlurMask(_codeElement, BlurMaskFilter.Blur.NORMAL);

        _toggleElement = (Switch) findViewById(R.id.status);
        _toggleElement.setChecked(_sharedPref.getBoolean("status", true));

        _toggleAvatarElement = (Switch) findViewById(R.id.avatar);
        _toggleAvatarElement.setChecked(_sharedPref.getBoolean("avatar", false));

        checkPermission();
        initEvents();
    }

    protected String getCode() {
        String defaultSecurityCode = _sharedPref.getString("securityCode", "0");

        if (defaultSecurityCode.equalsIgnoreCase("0")) {
            SecureRandom random = new SecureRandom();
            String securityCode = new BigInteger(130, random).toString(32).substring(0, 9).toUpperCase();
            SharedPreferences.Editor editor = _sharedPref.edit();
            editor.putString("securityCode", securityCode);
            editor.commit();
            defaultSecurityCode = _sharedPref.getString("securityCode", securityCode);
        }
        return defaultSecurityCode;
    }

    protected void initEvents() {
        _generateElement.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecureRandom random = new SecureRandom();
                String securityCode = new BigInteger(130, random).toString(32).substring(0, 9).toUpperCase();
                SharedPreferences.Editor editor = _sharedPref.edit();
                editor.putString("securityCode", securityCode);
                editor.commit();

                _codeElement.setText(securityCode);
            }
        });

        _toggleElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = _sharedPref.edit();
                if (_toggleElement.isChecked()) {
                    editor.putBoolean("status", true);
                    editor.commit();
                } else {
                    editor.putBoolean("status", false);
                    editor.commit();
                }
            }
        });

        _toggleAvatarElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = _sharedPref.edit();
                if (_toggleAvatarElement.isChecked()) {
                    editor.putBoolean("avatar", true);
                    editor.commit();
                } else {
                    editor.putBoolean("avatar", false);
                    editor.commit();
                }
            }
        });

        _codeElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _codeElement.getPaint().reset();
                _codeElement.getPaint().set(_textPaint);
                _codeElement.setText(getCode());
                _codeElement.setTextAppearance(getApplicationContext(), android.R.style.TextAppearance_Large);
            }
        });
    }

    protected void checkPermission() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_PHONE_STATE)) {
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_PHONE_STATE},
                            0);
                }
            }
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_CONTACTS)) {
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            0);
                }
            }
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.RECEIVE_SMS)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.RECEIVE_SMS)) {
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.RECEIVE_SMS},
                            0);
                }
            }
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_SMS)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_SMS)) {
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_SMS},
                            0);
                }
            }
        }
    }

    protected void applyBlurMask(TextView textView, BlurMaskFilter.Blur style) {
        if (Build.VERSION.SDK_INT >= 11) {
            textView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        float radius = textView.getTextSize() / 3;
        BlurMaskFilter filter = new BlurMaskFilter(radius, style);
        textView.getPaint().setMaskFilter(filter);
    }
}
