package com.piatrouski.com.ringingnotification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

public class SmsNotificationReceiver extends BroadcastReceiver {

    final SmsManager sms = SmsManager.getDefault();

    public SmsNotificationReceiver() {
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() == null) {
            return;
        }
        try {
            Object[] pdusObj = (Object[]) intent.getExtras().get("pdus");
            for (int i = 0; i < pdusObj.length; i++) {
                SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                String incomingNumber = currentMessage.getDisplayOriginatingAddress();
                String message = currentMessage.getDisplayMessageBody();
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
                String securityCode = sharedPref.getString("securityCode", "0");
                if (sharedPref.getBoolean("status", true)) {
                    NotifyWebService notify = new NotifyWebService();
                    notify.setContext(context);
                    notify.execute(securityCode, "sms", incomingNumber, message);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return;
    }
}
