package com.piatrouski.com.ringingnotification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

public class RingingNotificationReceiver extends BroadcastReceiver {

    private static Boolean _notifyState = false; // for prevent send twice states on lolipop

    public RingingNotificationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

        if (state == null) {
            return;
        }

        if ((state.equals(TelephonyManager.EXTRA_STATE_RINGING)) && (!_notifyState)) {
            _notifyState = true;
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
            String securityCode = sharedPref.getString("securityCode", "0");
            if (sharedPref.getBoolean("status", true)) {
                NotifyWebService notify = new NotifyWebService();
                notify.setContext(context);
                notify.execute(securityCode, "call", incomingNumber, "");
            }
        }

        if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
            _notifyState = false;
        }
    }


}
